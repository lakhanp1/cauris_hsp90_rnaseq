library(tidyverse)
library(ggplot2)
library(scales)

## 1) read the GO SLIM result table for two samples and generate a comparative statistics
## 2) generate a plot showing the comparative statistics


rm(list = ls())

path <- "E:/Chris_UM/Analysis/26_Cowen_CAuris_RNAseq/"

setwd(path)

## "HSP90 inhibition (GdA)", "HSP90 depletion (DOX)"

condn1 <- "HSP90_GdA"
condn1Name <- "HSP90 inhibition (GdA)"
file_slim1 <- "CAlb_diff/CAlb_WT_YPD_GdA_vs_WT_YPD/functional_analysis/CAlb_HSP90_GdA_GOSLIM.tab"
# "CAlb_diff/CAlb_WT_YPD_GdA_vs_WT_YPD/functional_analysis/CAlb_HSP90_GdA_GOSLIM.tab"
# "CAuris_diff/CAur_WT_YPD_GdA_vs_WT_YPD/functional_analysis/CAur_HSP90_GdA_GOSLIM.tab"

condn2 <- "HSP90_DOX"
condn2Name <- "HSP90 depletion (DOX)"
file_slim2 <- "CAlb_diff/CAlb_HSP90_depletion_effect/functional_analysis/CAlb_HSP90_DOX_GOSLIM.tab"
# "CAlb_diff/CAlb_HSP90_depletion_effect/functional_analysis/CAlb_HSP90_DOX_GOSLIM.tab"
# "CAuris_diff/CAur_HSP90_depletion_effect/functional_analysis/CAur_HSP90_DOX_GOSLIM.tab"

title = paste("C. albicans GO SLIM comparison:", condn1Name, "and", condn2Name)

name = "CAlb_DOX_vs_GdA_GO_SLIM_compare"
outdir <- "comparative_analysis/CAlbicans_HSP90_DOX_vs_GdA"
## CAur_DOX_vs_GdA_GO_SLIM_compare    CAuris_HSP90_DOX_vs_GdA    
## CAlb_DOX_vs_GdA_GO_SLIM_compare    CAlbicans_HSP90_DOX_vs_GdA   

outPrefix <- paste(outdir, name, sep = "/")

###########################################################################
## read the GO SLIM mapping results
slim1 <- suppressMessages(read_tsv(file = file_slim1, col_names = T)) %>% 
  dplyr::mutate(id = paste(sig_DEG, GOID, sep = "_"))

## convert datafram to list structure
slim1L <- list()
for (i in 1:nrow(slim1)) {
  slim1L[[ slim1$sig_DEG[i] ]][[ slim1$GOID[i] ]] <- list(
    id = slim1$GOID[i],
    term = slim1$TERM[i],
    count = slim1$count[i],
    genes = unlist(strsplit(x = slim1$genes[i], split = ";", fixed = TRUE))
  )
}



slim2 <- suppressMessages(read_tsv(file = file_slim2, col_names = T)) %>% 
  dplyr::mutate(id = paste(sig_DEG, GOID, sep = "_"))

slim2L <- list()
for (i in 1:nrow(slim2)) {
  slim2L[[ slim2$sig_DEG[i] ]][[ slim2$GOID[i] ]] <- list(
    id = slim2$GOID[i],
    term = slim2$TERM[i],
    count = slim2$count[i],
    genes = unlist(strsplit(x = slim2$genes[i], split = ";", fixed = TRUE))
  )
}

###########################################################################
## up regulated genes GO SLIM comparison between two conditions

## get overlap from two SLIM results
upCmpDf <- map2_dfr(
  .x = slim1L$up,
  .y = slim2L$up,
  .f = function(a, b, n1, n2){
    ## check if the two lists are parallel
    if(a$id != b$id){
      stop("List elements are not is same order: ", a$id, b$id)
    }
    
    dt <- list()
    
    dt[["id"]] <- a$id
    dt[["term"]] <- a$term
    dt[["total"]] <- length(na.exclude(union(a$genes, b$genes)))
    
    if(dt[["total"]] == 0){
      dt[["common"]] <- NA
      dt[[n1]] <- NA
      dt[[n2]] <- NA
    } else{
      dt[["common"]] <- length(intersect(a$genes, b$genes))
      dt[[n1]] <- length(na.exclude(setdiff(a$genes, b$genes)))
      dt[[n2]] <- length(na.exclude(setdiff(b$genes, a$genes)))
    }
    
    return(dt)
    
  },
  n1 = condn1, n2 = condn2) %>% 
  dplyr::mutate(deg_group = "up")


## plot data
upCmpPltDt <- tidyr::gather(data = upCmpDf,
                            key = "group", value = "count",
                            common, !! as.name(condn1), !! as.name(condn2),
                            factor_key = FALSE)

upCmpPltDt$group = factor(upCmpPltDt$group, levels = c(condn1, "common", condn2))

upPlt <- ggplot(data = upCmpPltDt, mapping = aes(x = term, y = count, fill = group)) +
  geom_bar(stat = 'identity', position = "fill", color = "black") +
  coord_flip() +
  scale_y_continuous(labels = scales::percent) +
  scale_fill_manual(
    name = "",
    breaks = c(condn1, "common", condn2),
    labels = c(condn1Name, "common", condn2Name),
    values = c("#ff4d4d", "white", "#0080ff")
  ) +
  ggtitle(str_wrap(title, 80)) +
  ylab("Relative %") +
  theme_bw() +
  theme(plot.title = element_text(hjust = 0.8, size = 14, face = "bold"),
        axis.text.x = element_text(size = 13),
        axis.text.y = element_text(size = 13),
        axis.title.x = element_text(face = "bold"),
        axis.title.y = element_blank(),
        legend.text = element_text(size = 13),
        legend.title = element_text(size = 13, face = "bold"))


###########################################################################
## GO SLIM comparison for down regulated genes

## get overlap from two SLIM results
downCmpDf <- map2_dfr(
  .x = slim1L$down,
  .y = slim2L$down,
  .f = function(a, b, n1, n2){
    ## check if the two lists are parallel
    if(a$id != b$id){
      stop("List elements are not is same order: ", a$id, b$id)
    }
    
    dt <- list()
    
    dt[["id"]] <- a$id
    dt[["term"]] <- a$term
    dt[["total"]] <- length(na.exclude(union(a$genes, b$genes)))
    
    if(dt[["total"]] == 0){
      dt[["common"]] <- NA
      dt[[n1]] <- NA
      dt[[n2]] <- NA
    } else{
      dt[["common"]] <- length(intersect(a$genes, b$genes))
      dt[[n1]] <- length(na.exclude(setdiff(a$genes, b$genes)))
      dt[[n2]] <- length(na.exclude(setdiff(b$genes, a$genes)))
    }

    return(dt)
    
  },
  n1 = condn1, n2 = condn2) %>% 
  dplyr::mutate(deg_group = "down")

###########################################################################
## GO SLIM comparison for both up and down regulated gene groups
allCmpDf <- dplyr::bind_rows(upCmpDf, downCmpDf)

write_tsv(x = allCmpDf, path = paste(outPrefix, "_data.tab", sep = ""), col_names = T)

## merge the up and down SLIM data
allSlimDf <- tidyr::gather(data = allCmpDf,
                           key = "group", value = "count",
                           common, !! as.name(condn1), !! as.name(condn2),
                           factor_key = FALSE)

allSlimDf$group = factor(allSlimDf$group, levels = c(condn1, "common", condn2))
allSlimDf$deg_group = factor(allSlimDf$deg_group, levels = c("up", "down"))


allCmpPlt <- ggplot(data = allSlimDf, mapping = aes(x = term, y = count, fill = group)) +
  geom_bar(stat = 'identity', position = "fill", color = "black") +
  coord_flip() +
  scale_y_continuous(labels = scales::percent) +
  scale_fill_manual(
    name = "",
    breaks = c(condn1, "common", condn2),
    labels = c(condn1Name, "common", condn2Name),
    values = c("#ff4d4d", "white", "#0080ff"),
    na.value = "grey"
  ) +
  ggtitle(str_wrap(title, 80)) +
  ylab("Relative %") +
  theme_bw() +
  theme(plot.title = element_text(hjust = 1, size = 14, face = "bold"),
        axis.text.x = element_text(size = 13),
        axis.text.y = element_text(size = 13),
        axis.title.x = element_text(face = "bold"),
        axis.title.y = element_blank(),
        panel.spacing = unit(1.5, "lines"),
        strip.text = element_text(hjust = 0.5, size = 14, face = "bold"),
        strip.background = element_rect(fill="white"),
        legend.text = element_text(size = 13),
        legend.position = "bottom",
        legend.title = element_text(size = 13, face = "bold"),
        plot.margin = unit(rep(0.5, 4), "cm")) +
  facet_grid(. ~ deg_group)


png(filename = paste(outPrefix, "_barplot.png", sep = ""), width=6000, height=5000, res = 500)
print(allCmpPlt)
dev.off()





