library(GO.db)
library(org.Calbicans.eg.db)
library(org.Cauris.eg.db)
library(ggplot2)
library(scales)
library(tidyverse)

## 1) group the up and down regulated genes into different GO SLIM terms
## 2) generate a bar chart of % of significant DEGs belonging to each term

rm(list = ls())

source(file = "E:/Chris_UM/Codes/GO_enrichment/enrichment_functions.R")
source(file = "E:/Chris_UM/Codes/GO_enrichment/topGO_functions.R")

path <- "E:/Chris_UM/Analysis/26_Cowen_CAuris_RNAseq/CAlb_diff/CAlb_HSP90_depletion_effect/"

setwd(path)


file_deg <- "CAlb_HSP90_depletion_effect_DEG_all.txt"

orgDb <- org.Calbicans.eg.db

outdir <- "functional_analysis"
name <- "CAlb_HSP90_DOX_GOSLIM"
titleName <- "CAlbicans HSP90 depletion (DOX) effect: GO SLIM"
outPrefix <- paste(outdir, name, sep = "/")



slimIds <- c("GO:0006810", "GO:0042221", "GO:0006950", "GO:0050789", "GO:0005975", "GO:0030447", "GO:0042493", "GO:0044419", "GO:0006629", "GO:0009405", "GO:0006996", "GO:0019725", "GO:0006091", "GO:0042710", "GO:0006464", "GO:0016070", "GO:0007049", "GO:0006259", "GO:0045333", "GO:0071555", "GO:0030163", "GO:0006766", "GO:0007155", "GO:0048468", "GO:0016192", "GO:0000746", "GO:0070783", "GO:0006457", "GO:0007165", "GO:0030448", "GO:0007124", "GO:0006412", "GO:0007114", "GO:0042254", "GO:0000910", "GO:0007010", "GO:0032196", "GO:0006997")

otherIds <- c("GO:0030447", "GO:0030448", "GO:0007124", "GO:0001411", "GO:0009405", "GO:0005618", "GO:0071555")

goIds <- unique(c(slimIds, otherIds))

p_cutoff <- 0.05
lfc_cut <- 0.585
up_cut <- lfc_cut
down_cut <- lfc_cut * -1


###########################################################################

## prepare DEG data based on LFC cutoff
degData <- data.table::fread(file = file_deg, sep = "\t", header = T, na.strings = "NA", stringsAsFactors = F) %>% 
  dplyr::mutate(sig_DEG = case_when(
    padj < p_cutoff & log2FoldChange >= up_cut ~ "up",
    padj < p_cutoff & log2FoldChange <= down_cut ~ "down",
    TRUE ~ "noDEG"
  )) %>% 
  dplyr::arrange(desc(log2FoldChange)) %>% 
  dplyr::filter(!is.na(log2FoldChange))


goMapings <- dplyr::filter(degData, sig_DEG != "noDEG") %>% 
  dplyr::group_by(sig_DEG) %>% 
  dplyr::do(GO_map(genes = .$geneId, goTerms = goIds, org = orgDb)) %>% 
  dplyr::ungroup()


write_tsv(x = goMapings, path = paste(outPrefix, ".tab", sep = ""), col_names = T)

###########################################################################
## bar plot
plotDf <- dplyr::mutate(goMapings,
                        percentage = 100 * count / inputSize
                        )


cols = c("up" = "#E69F00", "down" = "#56B4E9")

p1 = ggplot(data = plotDf, mapping = aes(x = TERM, y = percentage)) +
  geom_bar(mapping = aes(fill = sig_DEG), stat="identity", position=position_dodge()) +
  scale_fill_manual(
    name = "DEGs",
    breaks = c("up", "down"),
    labels = c("Up", "Down"),
    values = cols) +
  coord_flip() +
  ggtitle(str_wrap(titleName, 80)) +
  ylab("Percent of genes") +
  xlab("GO SLIM term") +
  theme_bw() +
  theme(plot.title = element_text(hjust = 1, size = 14, face = "bold"),
        axis.text.x = element_text(size = 13),
        axis.text.y = element_text(size = 15),
        axis.title = element_text(face = "bold"),
        legend.text = element_text(size = 13),
        legend.title = element_text(size = 13, face = "bold"))


png(filename = paste(outPrefix, "_barplot.png", sep = ""), width=5000, height=6000, res = 500)
print(p1)
dev.off()


###########################################################################


